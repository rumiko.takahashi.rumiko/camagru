-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: camagru
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_token`
--

DROP TABLE IF EXISTS `access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_token` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `token` text,
  `status` varchar(30) NOT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_token`
--

LOCK TABLES `access_token` WRITE;
/*!40000 ALTER TABLE `access_token` DISABLE KEYS */;
INSERT INTO `access_token` VALUES (16,'b01d78530c4a9f7eba5d5581d002329e4cccb2391381b426e3a4a53f1c475e4a18a5462b4f382f4d9318df79ebbc356eac13fe84c69e8d96750b24b69a3baf7d','1','30'),(17,'b01d78530c4a9f7eba5d5581d002329e4cccb2391381b426e3a4a53f1c475e4a18a5462b4f382f4d9318df79ebbc356eac13fe84c69e8d96750b24b69a3baf7d','1','31');
/*!40000 ALTER TABLE `access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_img` int(11) NOT NULL,
  `id_sender` int(11) NOT NULL,
  `id_owner` int(11) NOT NULL,
  `comment_text` text NOT NULL,
  `comment_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,11,1,5,'COmment','2018-11-18 14:24:32'),(2,11,1,5,'COmment','2018-11-18 14:25:25'),(3,10,1,5,'Add comment','2018-11-18 14:25:37'),(4,10,1,5,'New comment','2018-11-18 14:25:53'),(5,11,1,5,'asdf','2018-11-18 14:34:27'),(6,11,1,5,'asdf','2018-11-18 14:34:31'),(7,11,1,5,'loh Rabotaet','2018-11-18 16:03:45'),(8,11,1,5,'rabotaet','2018-11-18 16:04:10'),(25,11,1,5,'Test roboty','2018-11-18 16:17:30'),(26,11,1,5,'Test roboty','2018-11-18 16:17:48'),(27,11,1,5,'Test roboty','2018-11-18 16:17:57'),(28,12,1,5,'Loh','2018-11-18 16:18:09'),(29,1,1,1,'Test tut','2018-11-18 16:41:08'),(30,1,1,1,'Test 2','2018-11-18 16:44:04'),(31,11,1,5,'Last comment ','2018-11-18 16:44:24'),(32,11,30,5,'asdfasdfasdf','2019-12-24 11:55:49'),(33,11,30,5,'asdfasdfasdf','2019-12-24 11:56:10'),(34,11,30,5,'asdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdfasdfasdfasdfasd asdf','2019-12-24 11:56:21'),(35,11,30,5,'asdfasdf','2019-12-24 11:57:43'),(36,11,30,5,'\r\nComment :rabotaet\r\n\r\nAuthor :\r\n\r\n\r\n','2019-12-24 12:01:22'),(37,11,30,5,'\r\nComment :rabotaet\r\n\r\nAuthor :\r\n\r\n\r\n','2019-12-24 12:01:48'),(38,11,30,5,'\r\nComment :rabotaet\r\n\r\nAuthor :\r\n\r\n\r\n','2019-12-24 12:02:04'),(39,11,30,5,'\r\nComment :rabotaet\r\n\r\nAuthor :\r\n\r\n\r\n','2019-12-24 12:02:19'),(40,11,30,5,'\r\nComment :rabotaet\r\n\r\nAuthor :\r\n\r\n\r\n','2019-12-24 12:02:29'),(41,26,30,30,'TEst','2020-01-17 15:04:30'),(42,26,30,30,'asdf','2020-01-17 15:22:27'),(43,26,30,30,'asdf','2020-01-17 15:23:04'),(44,26,30,30,'asdf','2020-01-17 15:23:16'),(45,26,30,30,'asdf','2020-01-17 15:23:19'),(46,26,30,30,'asdf','2020-01-17 15:24:01'),(47,26,30,30,'asdf','2020-01-17 15:25:01'),(48,26,30,30,'asdf','2020-01-17 15:25:03'),(49,26,30,30,'asdf','2020-01-17 15:25:35'),(50,26,30,30,'asdf','2020-01-17 15:26:14'),(51,26,30,30,'asdf','2020-01-17 15:26:19'),(52,26,30,30,'asdf','2020-01-17 15:27:13'),(53,26,30,30,'asdf','2020-01-17 15:27:26'),(54,26,30,30,'asdf','2020-01-17 15:27:32'),(55,26,30,30,'asdf','2020-01-17 15:28:45'),(56,26,30,30,'1231234','2020-01-17 15:46:18'),(57,26,30,30,'1231234','2020-01-17 15:46:33'),(58,26,30,30,'asdfasd12312312341234','2020-01-17 15:51:59'),(59,26,30,30,'asdf','2020-01-17 15:59:22'),(60,26,30,30,'asdfasdf1231234123','2020-01-17 15:59:42'),(61,26,30,30,'FInal Test ','2020-01-17 16:03:27'),(62,26,30,30,'Final Comment','2020-01-17 16:04:56'),(63,26,30,30,'Final Comment 1','2020-01-17 16:05:09'),(64,26,30,30,'Final Comment 2\n','2020-01-17 16:05:17'),(65,26,30,30,'Final Test 1','2020-01-17 16:08:16'),(66,26,30,30,'Final Test 2','2020-01-17 16:08:36'),(67,12,30,5,'KEK\n','2020-01-17 16:10:13'),(68,12,30,5,'KEKEKE','2020-01-17 16:13:09'),(69,13,30,30,'1123','2020-01-17 16:31:42'),(70,29,30,30,'SUKa\n','2020-01-20 10:49:47'),(71,29,30,30,'TI loH','2020-01-20 10:49:56'),(72,14,30,30,'asdf','2020-01-20 14:40:48'),(73,27,30,30,'asdf','2020-01-20 14:46:46'),(74,27,30,30,'asdf','2020-01-20 14:46:48'),(75,28,30,30,'asdf','2020-01-20 14:47:11'),(76,29,30,30,'asdf','2020-01-20 15:23:53'),(77,27,30,30,'aaa','2020-01-20 15:24:13'),(78,27,30,30,'asdf','2020-01-20 15:24:21'),(79,31,30,30,'First Comment\n','2020-01-20 15:53:21');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `name_img` varchar(255) DEFAULT NULL,
  `description_img` varchar(255) DEFAULT NULL,
  `likes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,1,':name_imge',':description_img',NULL),(2,1,'Test Name','Test Description',NULL),(3,1,'Test Name 222','Test Description 222',NULL),(4,1,'Test Name 444','Test Description . 4444',NULL),(5,5,'New image','Description for new images',NULL),(6,5,'My photo','This is my photo',NULL),(7,5,'Test Name','Test Description',NULL),(8,5,'Lol','LOL',NULL),(9,5,'asdfaasdf','Test Description',NULL),(10,5,'asdfaasdf','Test Description',NULL),(11,5,'new_photo','Text',NULL),(12,5,'Test Name','Test Description',NULL),(13,30,'','','[{\"user_id\":\"30\",\"like\":\"1\"},{\"user_id\":\"31\",\"like\":\"0\"}]'),(14,30,'New test ','New test ','[{\"user_id\":\"31\",\"like\":\"1\"},{\"user_id\":\"30\",\"like\":\"1\"}]'),(30,30,'asdf','asdf',NULL),(31,30,'asdf','asdf','[{\"user_id\":\"31\",\"like\":\"1\"}]'),(34,30,'12','12','[{\"user_id\":\"30\",\"like\":\"1\"},{\"user_id\":\"31\",\"like\":\"1\"}]');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  `notification` varchar(255) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (30,'Tady','budnik245@gmail.com','45728febe19d8abf94624883776d5d8f7b54f1b2efad770ea9c9593ca6345fd57878fd59c2f5c37dd07711d1c82f1f406fe4ad8dcf3668ebc9f29935b25c52a4','1','0'),(31,'Tady','budnik@gmail.com','45728febe19d8abf94624883776d5d8f7b54f1b2efad770ea9c9593ca6345fd57878fd59c2f5c37dd07711d1c82f1f406fe4ad8dcf3668ebc9f29935b25c52a4','1','0');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-21 18:26:17
