<?php

namespace application\models;

class Account
{
    public static function sendNotificationEmail($email, $photoId)
    {
        try {
            $to_email = $email;
            $subject = 'New Like!!!';
            $message = "New like. Photo Id : " . $photoId;
            $message .= "\n";
            $headers = 'From: noreply@camagru.com';
            $success = mail($to_email, $subject, $message, $headers);
            if (!$success) {
                $errorMessage = error_get_last()['message'];
                mailerLog($errorMessage);
            }
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        }
    }
}
