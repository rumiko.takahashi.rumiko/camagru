<h3>Accepted registration</h3>
<div class="error" id="error-message">
    <ul id="error-message-list" style="background-color: transparent;"></ul>
</div>

<div class="login-form">
    <form action="javascript:void(0);" method="post">
        <label for="email">E-mail</label>
        <input type="email" name="email" id="email" placeholder="E-mail" value=""/>
        <label for="text">Password</label>
        <input type="password" name="password" id="password" placeholder="Pass" value=""/>
        <br>
        <input type="submit" name="submit" id="submit" value="Login"/>
    </form>
</div>

<script type="text/javascript">
    document.addEventListener("click", function() {
        var error_list = document.getElementById('error-message-list');
        if (error_list.innerText.length !== 0) {
            error_list.innerText = '';
        }
    });
    document.getElementById("submit").addEventListener("click", function(event) {
        event.preventDefault();
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        if (email === "" || password === "") {
            alert("Incorrect user data!!!");
            return false;
        }
        var data = "email="+email+"&password="+password+"&url="+document.URL;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>access", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length === 0) {
                    location.reload();
                } else {
                    var error_list = document.getElementById('error-message-list');
                    var response = JSON.parse(xhr.responseText);
                    error_list.innerHTML += "<li id='error-msg'>" + response +"</li><br>";
                }
            }
        };
    });
</script>