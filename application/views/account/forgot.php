<h3>Forgot password</h3>
<div class="error" id="error-message">
    <ul id="error-message-list" style="background-color: transparent;"></ul>
</div>

<div class="login-form">
    <form action="javascript:void(0);" method="post">
        <label for="email">E-mail</label>
        <input type="email" name="email" id="email" placeholder="E-mail" value=""/>
        <br>
        <input type="submit" name="submit" id="submit" value="Send new password"/>
    </form>
</div>

<script type="text/javascript">
    document.addEventListener("click", function() {
        var error_list = document.getElementById('error-message-list');
        if (error_list.innerText.length !== 0) {
            error_list.innerText = '';
        }
    });
    document.getElementById("submit").addEventListener("click", function(event) {
        event.preventDefault();
        var email = document.getElementById("email").value;
        if (email === "") {
            alert("Incorrect user data!!!");
            return false;
        }
        var data = "email="+email;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>account/forgot", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length === 0) {
                    var error_list = document.getElementById('error-message-list');
                    document.getElementById("submit").disabled = true;
                    error_list.innerHTML += "<li id='error-msg'><strong>We will send a new password to your email !!!</strong></li><br>";
                    setTimeout(() => {
                        window.location.href = "<?php echo BASE_URL ?>";
                    }, 5000);
                } else {
                    var error_list = document.getElementById('error-message-list');
                    var response = JSON.parse(xhr.responseText);
                    error_list.innerHTML += "<li id='error-msg'>" + response +"</li><br>";
                }
            }
        };
    });
</script>