<h2>Add photo</h2>

<div class="foto">
    <div id="upload-file">
        <form action="javascript:void(0);" method="post">
            <button style="width: 90px" type="button" id="make-photo" name="button"><img  width=50px src="<?php echo BASE_URL ?>public/img/zat.png" width=100% ></button><br/>
            <div class="download-photo">
                <label>File:</label><br/>
                <input class="dw" type="file" id="imageLoader" name="imageLoader" multiple accept="image/*"/>
            </div>
        </form>
    </div>

    <div class="video-container" style="overflow: hidden;">
        <div class="video-box" style="width: 1000%;">
            <div class="elem_foto_cam" id="video-element">
                <video id="video" width="800" height="600" autoplay="autoplay" />
            </div>
            <div id="canvas-element">
                <canvas id="canvas" width="800" height="600" />
            </div>
        </div>
    </div>
    <div id="el_4">
        <div class="container">
            <button type="button" id="sepia-button" name="sepia">Sepia</button>
            <button type="button" id="gray-button" name="gray">Gray</button>
            <button type="button" id="noise-button" name="noise">Noise</button>
            <button type="button" id="invert-button" name="invert">Invert</button>
            <button type="button" id="more-button" name="more">More</button>
            <button type="button" id="frame-button" name="button"><img  width="50px" src="<?php echo BASE_URL ?>public/img/w.png"></button>
            <button type="button" id="frame1-button" name="frame1"><img  height="50px" src="<?php echo BASE_URL ?>public/img/r1.png"></button>
            <button type="button" id="frame2-button" name="frame2"><img  height="50px" src="<?php echo BASE_URL ?>public/img/r4.png"></button>
            <button type="button" id="frame3-button" name="frame3"><img  width="50px" src="<?php echo BASE_URL ?>public/img/r5.png"></button>
            <button type="button" id="reset-photo" name="reset-photo">Reset</button>
            <div class="save">
                <button type="button" id="save-photo" name="save">Save</button>
                <div class="pop-up">
                    <div class="hover_bkgr_fricc" id="hover_bkgr_fricc">
                        <span class="helper"></span>
                        <div>
                            <div class="error" id="error-message">
                                <ul id="error-message-list" style="background-color: transparent;"></ul>
                            </div>
                            <div class="popupCloseButton" id="popupCloseButton">&times;</div>
                            <form action="javascript:void(0);" method="post">
                                <label for="name">Name image</label>
                                <input type="text" placeholder="Enter Name" name="name" id="name" required>
                                <label for="description">Description</label>
                                <input type="text" placeholder="Enter Description" name="description" id="description" required>
                                <input type="submit" name="submit" value="Save" id="submit"/>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    window.Photo = new Function();

    Photo.prototype = {
        initialize : function () {
            this.canvas         =       document.getElementById('canvas');
            this.video          =       document.getElementById('video');
            this.button         =       document.getElementById('make-photo');
            this.button1        =       document.getElementById('sepia-button');
            this.button2        =       document.getElementById('frame-button');
            this.button4        =       document.getElementById('gray-button');
            this.button5        =       document.getElementById('noise-button');
            this.button6        =       document.getElementById('invert-button');
            this.button7        =       document.getElementById('more-button');
            this.frame1         =       document.getElementById('frame1-button');
            this.frame2         =       document.getElementById('frame2-button');
            this.frame3         =       document.getElementById('frame3-button');
            this.save           =       document.getElementById('submit');
            this.context        =       this.canvas.getContext('2d');
            this.videoStreamUrl =       false;
            this.reset          =       document.getElementById('reset-photo');
            this.reset_img;
            this.imageLoader    =       document.getElementById('imageLoader');
            this.flag           =       false;
            this.trigetPopUp    =       document.getElementById('save-photo');
            this.trigetPopUpCls =       document.getElementById('popupCloseButton');

            this.initEvents();
            this.errorsHandler();
        },

        initEvents : function () {

            (function (a) {
                navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
                window.URL.createObjectURL = window.URL.createObjectURL || window.URL.webkitCreateObjectURL || window.URL.mozCreateObjectURL || window.URL.msCreateObjectURL;
                navigator.getUserMedia({video:true}, function(stream){
                    a.videoStreamUrl = stream;
                    a.video.srcObject = stream;
                },function(){
                    a.video.style.display = 'none';
                });
            })(this);

            this.trigetPopUp.addEventListener('click', function () {
                document.getElementById('hover_bkgr_fricc').style.display = "block";
            });

            this.trigetPopUpCls.addEventListener('click', function () {
                document.getElementById('hover_bkgr_fricc').style.display = 'none';
            });

            this.save.addEventListener('click', this.saveImg(this));
            this.button.addEventListener('click', this.capTureMe(this));
            this.reset.addEventListener('click', this.resetPhoto(this));
            this.button1.addEventListener('click', this.sepia(this));
            this.button2.addEventListener('click', this.logo(this));
            this.button4.addEventListener('click', this.grayscale(this));
            this.button5.addEventListener('click', this.nose(this));
            this.button6.addEventListener('click', this.invert(this));
            this.button7.addEventListener('click', this.more(this));
            this.imageLoader.addEventListener('change', this.loadLocalPhoto(this), false);

            (function(a) {
                a.frame1.addEventListener('click', function (e) {
                    var str = "<?php echo BASE_URL ?>public/img/r1.png";
                    if (a.flag === false)
                        return;
                    var frame = new Image();
                    frame.crossOrigin = "Anonymous";
                    frame.src = str;
                    a.context.drawImage(frame, 0, 0, 800, 600);
                }, false);

                a.frame2.addEventListener('click', function (e) {
                    var str = "<?php echo BASE_URL ?>public/img/r4.png";
                    if (a.flag === false)
                        return;
                    var frame = new Image();
                    frame.crossOrigin = "Anonymous";
                    frame.src = str;
                    a.context.drawImage(frame, 0, 0, 800, 600);
                }, false);

                a.frame3.addEventListener('click', function (e) {
                    var str = "<?php echo BASE_URL ?>public/img/r5.png";
                    if (a.flag === false)
                        return;
                    var frame = new Image();
                    frame.crossOrigin = "Anonymous";
                    frame.src = str;
                    a.context.drawImage(frame, 0, 0, 800, 600);
                }, false);
            })(this);

        },

        errorsHandler : function() {
            document.addEventListener("click", function() {
                var error_list = document.getElementById('error-message-list');
                if (error_list.getElementsByTagName('li').length > 1) {
                    error_list.innerText = '';
                }
                if (error_list.innerText.length !== 0) {
                    setTimeout(function () {
                        error_list.innerText = '';
                    }, 5000);
                }
            });
        },

        loadLocalPhoto : function(a) {
            return function() {
                a.flag = true;
                var reader = new FileReader();

                reader.onload = function (event) {
                    var img = new Image();
                    img.onload = function () {
                        a.canvas.width = a.video.width;
                        a.canvas.height = a.video.height;
                        a.context.drawImage(img, 0, 0, a.video.width, a.video.height);
                        a.reset_img = a.context.getImageData(0, 0, 800, 600);
                        a.context.setTransform(1, 0, 0, 1, 0, 0);
                    };
                    img.src = event.target.result;
                };
                reader.readAsDataURL(a.imageLoader.files[0]);
            }
        },

        capTureMe : function (a) {
            return function() {
                if (!a.videoStreamUrl) {
                    alert("Please allow use web cam!!!!");
                    return ;
                }
                a.flag = true;
                a.context.translate(a.canvas.width, 0);
                a.context.scale(-1, 1);
                a.context.drawImage(a.video, 0, 0, a.video.width, a.video.height);
                a.reset_img = a.context.getImageData(0, 0, 800, 600);
                a.context.setTransform(1, 0, 0, 1, 0, 0);
            }
        },

        sepia : function (a) {
            return function() {
                if (a.flag === false) {
                    return;
                }

                var r = [0, 0, 0, 1, 1, 2, 3, 3, 3, 4,
                        4, 4, 5, 5, 5, 6, 6, 7, 7, 7,
                        7, 8, 8, 8, 9, 9, 9, 9, 10, 10,
                        10, 10, 11, 11, 12, 12, 12, 12,
                        13, 13, 13, 14, 14, 15, 15, 16,
                        16, 17, 17, 17, 18, 19, 19, 20,
                        21, 22, 22, 23, 24, 25, 26, 27,
                        28, 29, 30, 31, 32, 33, 34, 35,
                        36, 37, 39, 40, 41, 42, 44, 45,
                        47, 48, 49, 52, 54, 55, 57, 59,
                        60, 62, 65, 67, 69, 70, 72, 74,
                        77, 79, 81, 83, 86, 88, 90, 92,
                        94, 97, 99, 101, 103, 107, 109,
                        111, 112, 116, 118, 120, 124, 126,
                        127, 129, 133, 135, 136, 140, 142,
                        143, 145, 149, 150, 152, 155, 157,
                        159, 162, 163, 165, 167, 170, 171,
                        173, 176, 177, 178, 180, 183, 184,
                        185, 188, 189, 190, 192, 194, 195,
                        196, 198, 200, 201, 202, 203, 204,
                        206, 207, 208, 209, 211, 212, 213,
                        214, 215, 216, 218, 219, 219, 220,
                        221, 222, 223, 224, 225, 226, 227,
                        227, 228, 229, 229, 230, 231, 232,
                        232, 233, 234, 234, 235, 236, 236,
                        237, 238, 238, 239, 239, 240, 241,
                        241, 242, 242, 243, 244, 244, 245,
                        245, 245, 246, 247, 247, 248, 248,
                        249, 249, 249, 250, 251, 251, 252,
                        252, 252, 253, 254, 254, 254, 255,
                        255, 255, 255, 255, 255, 255, 255,
                        255, 255, 255, 255, 255, 255, 255,
                        255, 255, 255, 255, 255, 255, 255,
                        255, 255, 255, 255, 255, 255, 255],

                    g = [0, 0, 1, 2, 2, 3, 5, 5, 6, 7, 8,
                        8, 10, 11, 11, 12, 13, 15, 15, 16,
                        17, 18, 18, 19, 21, 22, 22, 23, 24,
                        26, 26, 27, 28, 29, 31, 31, 32, 33,
                        34, 35, 35, 37, 38, 39, 40, 41, 43,
                        44, 44, 45, 46, 47, 48, 50, 51, 52,
                        53, 54, 56, 57, 58, 59, 60, 61, 63,
                        64, 65, 66, 67, 68, 69, 71, 72, 73,
                        74, 75, 76, 77, 79, 80, 81, 83, 84,
                        85, 86, 88, 89, 90, 92, 93, 94, 95,
                        96, 97, 100, 101, 102, 103, 105, 106,
                        107, 108, 109, 111, 113, 114, 115, 117,
                        118, 119, 120, 122, 123, 124, 126, 127,
                        128, 129, 131, 132, 133, 135, 136, 137,
                        138, 140, 141, 142, 144, 145, 146, 148,
                        149, 150, 151, 153, 154, 155, 157, 158,
                        159, 160, 162, 163, 164, 166, 167, 168,
                        169, 171, 172, 173, 174, 175, 176, 177,
                        178, 179, 181, 182, 183, 184, 186, 186,
                        187, 188, 189, 190, 192, 193, 194, 195,
                        195, 196, 197, 199, 200, 201, 202, 202,
                        203, 204, 205, 206, 207, 208, 208, 209,
                        210, 211, 212, 213, 214, 214, 215, 216,
                        217, 218, 219, 219, 220, 221, 222, 223,
                        223, 224, 225, 226, 226, 227, 228, 228,
                        229, 230, 231, 232, 232, 232, 233, 234,
                        235, 235, 236, 236, 237, 238, 238, 239,
                        239, 240, 240, 241, 242, 242, 242, 243,
                        244, 245, 245, 246, 246, 247, 247, 248,
                        249, 249, 249, 250, 251, 251, 252, 252,
                        252, 253, 254, 255],

                    b = [53, 53, 53, 54, 54, 54, 55, 55, 55, 56,
                        57, 57, 57, 58, 58, 58, 59, 59, 59, 60,
                        61, 61, 61, 62, 62, 63, 63, 63, 64, 65,
                        65, 65, 66, 66, 67, 67, 67, 68, 69, 69,
                        69, 70, 70, 71, 71, 72, 73, 73, 73, 74,
                        74, 75, 75, 76, 77, 77, 78, 78, 79, 79,
                        80, 81, 81, 82, 82, 83, 83, 84, 85, 85,
                        86, 86, 87, 87, 88, 89, 89, 90, 90, 91,
                        91, 93, 93, 94, 94, 95, 95, 96, 97, 98,
                        98, 99, 99, 100, 101, 102, 102, 103, 104,
                        105, 105, 106, 106, 107, 108, 109, 109, 110,
                        111, 111, 112, 113, 114, 114, 115, 116, 117,
                        117, 118, 119, 119, 121, 121, 122, 122, 123,
                        124, 125, 126, 126, 127, 128, 129, 129, 130,
                        131, 132, 132, 133, 134, 134, 135, 136, 137,
                        137, 138, 139, 140, 140, 141, 142, 142, 143,
                        144, 145, 145, 146, 146, 148, 148, 149, 149,
                        150, 151, 152, 152, 153, 153, 154, 155, 156,
                        156, 157, 157, 158, 159, 160, 160, 161, 161,
                        162, 162, 163, 164, 164, 165, 165, 166, 166,
                        167, 168, 168, 169, 169, 170, 170, 171, 172,
                        172, 173, 173, 174, 174, 175, 176, 176, 177,
                        177, 177, 178, 178, 179, 180, 180, 181, 181,
                        181, 182, 182, 183, 184, 184, 184, 185, 185,
                        186, 186, 186, 187, 188, 188, 188, 189, 189,
                        189, 190, 190, 191, 191, 192, 192, 193, 193,
                        193, 194, 194, 194, 195, 196, 196, 196, 197,
                        197, 197, 198, 199];

                var noise = 20;
                var imageData = a.context.getImageData(0, 0, a.canvas.width, a.canvas.height);

                for (var i = 0; i < imageData.data.length; i += 4) {
                    imageData.data[i] = r[imageData.data[i]];
                    imageData.data[i + 1] = g[imageData.data[i + 1]];
                    imageData.data[i + 2] = b[imageData.data[i + 2]];
                    if (noise > 0) {
                        var noise = Math.round(noise - Math.random() * noise);
                        for (var j = 0; j < 3; j++) {
                            var iPN = noise + imageData.data[i + j];
                            imageData.data[i + j] = (iPN > 255) ? 255 : iPN;
                        }
                    }
                }
                a.context.putImageData(imageData, 0, 0);
            }
        },

        grayscale : function (a) {
            return function () {
                var er = 0;
                var eg = 0;
                var eb = 0;
                var p1 = 0.6;
                var p2 = 0.6;
                var p3 = 0.6;
                func = 'grayscale';
                var imageData = a.context.getImageData(0, 0, a.canvas.width, a.canvas.height);
                var data = imageData.data;

                for (var i = 0, n = data.length; i < n; i += 4) {
                    var grayscale = data[i] * p1 + data[i + 1] * p2 + data[i + 2] * p3;
                    data[i] = grayscale + er;
                    data[i + 1] = grayscale + eg;
                    data[i + 2] = grayscale + eb;
                }
                a.context.putImageData(imageData, 0, 0);
            }
        },

        nose : function (a) {
            return function () {
                var er = 0.1;
                var eg = 0.1;
                var eb = 0.1;
                var p2 = 1.2;
                var p3 = 1.2;
                var imageData = a.context.getImageData(0, 0, a.canvas.width, a.canvas.height);
                var data = imageData.data;

                for (var i = 0, n = data.length; i < n; i += 4) {
                    var randColor1 = 0.6 + Math.random() * 0.4;
                    var randColor2 = 0.6 + Math.random() * 0.4;
                    var randColor3 = 0.6 + Math.random() * 0.4;

                    data[i] = data[i] * p2 * randColor1 + er;
                    data[i + 1] = data[i + 1] * p2 * randColor2 + eg;
                    data[i + 2] = data[i + 2] * p3 * randColor3 + eb;
                }
                a.context.putImageData(imageData, 0, 0);
            }
        },

        invert : function (a) {
            return function () {
                var imageData = a.context.getImageData(0, 0, a.canvas.width, a.canvas.height);
                var data = imageData.data;

                for (var i = 0, n = data.length; i < n; i += 4) {

                    data[i] = 255 - data[i];
                    data[i + 1] = 255 - data[i + 1];
                    data[i + 2] = 255 - data[i + 2];
                }
                a.context.putImageData(imageData, 0, 0);
            }
        },

        more : function (a) {
            return function () {
                var iBlurRate = 1.3;
                var er = 0;
                var eg = 0;
                var eb = 0;
                var p1 = 1;
                var p2 = 1;
                var p3 = 1;

                var imageData = a.context.getImageData(0, 0, a.canvas.width, a.canvas.height);
                var data = imageData.data;
                for (br = 0; br < iBlurRate; br += 1) {
                    for (var i = 0, n = data.length; i < n; i += 4) {
                        iMW = 4 * a.canvas.width;
                        iSumOpacity = iSumRed = iSumGreen = iSumBlue = 0;
                        iCnt = 0;
                        aCloseData = [
                            i - iMW - 4, i - iMW, i - iMW + 4,
                            i - 4, i + 4,
                            i + iMW - 4, i + iMW, i + iMW + 4
                        ];
                        for (e = 0; e < aCloseData.length; e += 1) {
                            if (aCloseData[e] >= 0 && aCloseData[e] <= data.length - 3) {
                                iSumOpacity += data[aCloseData[e]];
                                iSumRed += data[aCloseData[e] + 1];
                                iSumGreen += data[aCloseData[e] + 2];
                                iSumBlue += data[aCloseData[e] + 3];
                                iCnt += 1;
                            }
                        }

                        data[i] = (iSumOpacity / iCnt) * p1 + er;
                        data[i + 1] = (iSumRed / iCnt) * p2 + eg;
                        data[i + 2] = (iSumGreen / iCnt) * p3 + eb;
                        data[i + 3] = (iSumBlue / iCnt);
                    }
                }
                a.context.putImageData(imageData, 0, 0);
            }
        },


        logo : function (a) {
            return function () {
                if (a.flag === false)
                    return;
                var logo = new Image();
                logo.src = "<?php echo BASE_URL ?>public/img/w.png";
                a.context.drawImage(logo, 240, 400, 200, 350);
            }
        },

        saveImg : function (a) {
            return function () {
                var name = document.getElementById("name").value;
                var description = document.getElementById("description").value;
                var error_list = document.getElementById('error-message-list');
                if (a.flag === false ||
                    (name.trim()).length === 0 ||
                    (description.trim()).length === 0)
                {
                    error_list.innerHTML += "<li id='error-msg'>Please check your data!</li><br>";
                    return;
                }
                var base64dataUrl = a.canvas.toDataURL('img/png');
                var xhr = new XMLHttpRequest();
                xhr.open("POST", "<?php echo BASE_URL ?>gallery/add", true);
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                xhr.send('image=' + encodeURIComponent(base64dataUrl) + "&name=" + name + "&description=" + description);
                xhr.onload = function() {
                    if (xhr.status != 200) {
                        alert("Problem with server! Contact with developer!!!")
                    } else {
                        if (xhr.response.length === 0) {
                            document.getElementById("name").value = '';
                            document.getElementById("description").value = '';
                            error_list.innerHTML += "<li id='error-msg'><strong>Your photo will be saved!!!</strong></li><br>";
                            setTimeout(() => {
                                window.location.href = "<?php echo BASE_URL ?>";
                            }, 5000);
                        } else {
                            error_list.innerHTML += "<li id='error-msg'>Problem with server! Contact with developer!!!</li><br>";
                        }
                    }
                };
            }
        },


        resetPhoto : function (a) {
            return function () {
                if (a.flag === false)
                    return;
                a.context.putImageData(a.reset_img, 0, 0);
            }
        },

    };

    var photo = new Photo();

    photo.initialize();
    window.photo = photo;
</script>
