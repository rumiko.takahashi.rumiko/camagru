<style>
    body {
        width:100%;
        margin:auto;
    }
    .gallery {
        width:100%;
        display:flex;
        flex-flow: row wrap;
    }
    .box {
        flex-basis:20%;
        width:100%;
        padding:10px;
        margin:8px;
    }

    @media only screen and (max-width: 300px) {
        .box {
            flex-basis: 100%;
        }

        .img {
            width: 150%;
            height: 150%;
        }
    }

    @media only screen and (max-width:500px) {
        .box {
            flex-basis: 40%;
        }

        .img {
            width: 150%;
            height: 150%;
        }
    }

</style>

<h2>Gallery</h2>

<div class="gallery-table">
    <?php $decodeLists = json_decode($vars['list']); ?>
    <?php $likeLists = $vars['like']; ?>
    <div class="gallery">
        <?php foreach ($decodeLists as $key => $val):?>
            <div class="box">
                <div class="image">
                    <?php if (file_exists(ROOT . '/public/img/photos/'. $val->id . '.jpg')): ?>
                        <a href="<?php echo BASE_URL ?>photo/<?php echo $val->id ?>">
                            <img class="img" src="<?php echo BASE_URL ?>public/img/photos/<?php echo $val->id ?>.jpg" width="400" height="400">
                        </a>
                        <span style="float:left;">Name: <?php echo substr($val->name_img, 0, 10); ?></span>
                        <span style="float:right;">like: <?php echo $likeLists[$key]; ?></span>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="pagination">
    <?php echo $vars['pagination']; ?>
</div>