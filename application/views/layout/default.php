<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="http://localhost:8080/public/styles/style.css">
    <title><?php echo $title; ?></title>
</head>
<body>
<div class="container">
    <div class="nav-bar">
        <ul>
            <li><a href="<?php echo BASE_URL ?>">Camagru</a></li>
            <?php if (!isset($_SESSION['user'])): ?>
                <li><a href="<?php echo BASE_URL ?>">Home</a></li>
            <?php endif;?>
            <?php if (isset($_SESSION['user'])): ?>
                <li><a href="<?php echo BASE_URL ?>gallery">Gallery</a></li>
            <?php endif;?>
            <li><a href="<?php echo BASE_URL ?>contact">Contact</a></li>
            <li><a href="<?php echo BASE_URL ?>about">About</a></li>
            <?php if (!isset($_SESSION['user'])): ?>
                <li><a href="<?php echo BASE_URL ?>account/register" style="float: right">Register</a></li>
            <?php endif;?>
            <?php if (isset($_SESSION['user'])): ?>
                <li class="dropdown" style="float: right">
                    <a href="javascript:void(0)" class="dropbtn">Account</a>
                    <div class="dropdown-content">
                        <a href="<?php echo BASE_URL ?>account">Photo</a>
                        <a href="<?php echo BASE_URL ?>account/setting">Settings</a>
                        <a href="<?php echo BASE_URL ?>logout">logout</a>
                    </div>
                </li>
            <?php endif;?>
        </ul>
    </div>
    <?php echo $content;?>
</div>
<div class="footer">
    <hr>
    <p>&copy;2020 vbudnik<p>
</div>
</body>
</html>
