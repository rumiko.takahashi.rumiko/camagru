<h3>Contact Form</h3>
<div class="contact-form">
    <form action="javascript:void(0);" method="post">
        <label for="name">Name</label>
        <input type="text" id="name" name="name" placeholder="Your name..">
        <label for="email">E-mail</label>
        <input type="email" id="email" name="email" placeholder="Your e-mail">
        <label for="subject">Subject</label>
        <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>
        <input type="submit" name="submit" value="Send" id="submit-btm">
    </form>
</div>

<script>
    document.getElementById('submit-btm').addEventListener("click", function(event) {
        event.preventDefault();
        alert("Your message is send!");
        setTimeout(() => {
            window.location.href = "<?php echo BASE_URL ?>";
        }, 5000);
    });
</script>