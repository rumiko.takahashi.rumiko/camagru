<?php

return [
    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],

    'access' => [
        'controller' => 'access',
        'action' => 'index',
    ],

    'access/{token:\w+}' => [
        'controller' => 'access',
        'action' => 'index',
    ],

    'about' => [
        'controller' => 'about',
        'action' => 'index',
    ],

    'contact' => [
        'controller' => 'contact',
        'action' => 'index',
    ],

    'account' => [
        'controller' => 'account',
        'action' => 'index',
    ],

    'account/register' => [
        'controller' => 'account',
        'action' => 'register',
    ],

    'account/forgot' => [
        'controller' => 'account',
        'action' => 'forgot',
    ],

    'account/setting' => [
        'controller' => 'account',
        'action' => 'setting',
    ],

    'account/settingajax' => [
        'controller' => 'account',
        'action' => 'settingajax',
    ],

    'gallery' => [
        'controller' => 'gallery',
        'action' => 'index',
    ],

    'like' => [
        'controller' => 'gallery',
        'action' => 'like',
    ],

    'gallery/add' => [
        'controller' => 'gallery',
        'action' => 'add',
    ],

    'gallery/{page:\d+}' => [
        'controller' => 'gallery',
        'action' => 'index',
    ],

    'photo/{id:\d+}' => [
        'controller' => 'gallery',
        'action' => 'photo',
    ],

    'addComment' => [
        'controller' => 'gallery',
        'action' => 'comment',
    ],

    'logout' => [
        'controller' => 'main',
        'action' => 'logout',
    ],

];
