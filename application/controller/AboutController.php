<?php

namespace application\controller;

use application\core\Controller;

class AboutController extends Controller
{
    public function indexAction() {
        return $this->view->render("Camagru | About As");
    }
}
