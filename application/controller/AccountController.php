<?php

namespace application\controller;

use application\core\Controller;
use application\models\User;

class AccountController extends Controller
{
    public function indexAction()
    {
        if (User::checkLogin()) {
            $this->view->render('Camagru | Account');
            return true;
        }
        $this->view->redirect('/');
        return true;
    }

    public function settingAction()
    {
        if (User::checkLogin()) {
            $userId = User::getUserId();
            $userData = User::getUserById($userId);
            $vars = [
                'name' => $userData['name'],
                'email' => $userData['email'],
                'notification' => $userData['notification'],

            ];
            $this->view->render('Camagru | Setting', $vars);
            return true;
        }
        $this->view->redirect('/');
        return true;
    }

    public function settingajaxAction()
    {
        if (User::checkLogin() && !empty($_POST)) {

            $userId = User::getUserId();
            $errors = false;

            if (isset($_POST['checked'])) {
                $flag = $_POST['checked'];
                if ($errors != false) {
                    echo json_encode($errors);
                    return false;
                }
                User::updateNotification($flag, $userId);
                return true;
            }
            if (isset($_POST['name'])) {
                $name = $_POST['name'];
                if (strlen($name) <= 2 || strlen($name) >= 20){
                    $errors[] = 'Not correct name!';
                }
                if ($errors != false) {
                    echo json_encode($errors);
                    return false;
                }
                User::updateUserName($name, $userId);
                return true;
            }

            if (isset($_POST['email'])) {
                $email = $_POST['email'];
                if (!User::checkEmail($email)) {
                    $errors[] = 'Not correct email!';
                }
                if (User::checkEmailExists($email) == true) {
                    $errors[] = 'Email exist!';
                }
                if ($errors != false) {
                    echo json_encode($errors);
                    return false;
                }
                User::updateUserEmail($email, $userId);
                return true;
            }

            if (isset($_POST['password'])) {
                $password = $_POST['password'];
                if (!User::checkPassword($password)) {
                    $errors[] = 'Not correct password!';
                }
                if ($errors != false) {
                    echo json_encode($errors);
                    return false;
                }
                User::updateUserPassword($password, $userId);
                return true;
            }
            echo json_encode("Problem with server! Contact with developer!!!");
            return false;
        }
        $this->view->redirect('/');
        return true;
    }

    public function registerAction()
    {
        if (!User::checkLogin()) {
             if (isset($_POST['email']) && isset($_POST['psw'])
                && isset($_POST['pswrepeat']) && isset($_POST['name'])) {
                 $user['name'] = $_POST['name'];
                 $user['email'] = $_POST['email'];
                 $user['password'] = $_POST['psw'];
                 $user['pswrepeat'] = $_POST['pswrepeat'];
                 if (!User::checkUserData($user['email'], $user['password']) &&
                     $user['password'] == $user['pswrepeat']) {
                     $currentTime = microtime(true);
                     $token = $currentTime .  $user['password'] . $user['name'];
                     $string = hash('whirlpool', $token);
                     $userId = User::registerUser($user);
                     $res = User::setAccessToken($string, $userId);
                     User::sendRegistrationEmail($user['email'], $string);
                     reportLog("KEKKE");
                     return true;
                     exit;
                 } else {
                     echo json_encode("Incorrect user data, Please try again!!!");
                     return false;
                 }
             }
            $this->view->render('Camagru | Regiser');
            return true;
        } else {
            $this->view->redirect('/');
            return true;
        }
    }

    public function forgotAction()
    {
        if (!User::checkLogin()) {
            if (isset($_POST['email'])) {
                $email = $_POST['email'];
                if (User::checkEmailExists($email)) {
                    $password = User::generateRandomPassword(PASSWORD_LEN);
                    $userId = User::getUserIdByEmail($email);
                    User::updateUserPassword($password, $userId);
                    User::sendForgotPasswordEmail($email, $password);
                    return true;
                } else {
                    echo json_encode("Incorrect email, Please try again!!!");
                    return false;
                }
                $this->view->redirect('gallery/');
            }
            $this->view->render('Camagru | Forgot Password');
            return true;
        }
        $this->view->redirect('/');
        return true;
    }
}
