<?php

namespace application\controller;

use application\core\Controller;
use application\models\User;

class AccessController extends Controller
{
    public function indexAction()
    {
        if (!User::checkLogin()) {
            if (isset($_POST['email']) && isset($_POST['password']) &&
                !empty($_POST['email']) && !empty($_POST['password'])
            ) {
                $token = [];
                $email = $_POST['email'];
                $password = $_POST['password'];
                $token = explode("/", $_POST['url']);
                if (User::checkToken($token[4]) != 0) {
                    echo json_encode("Token already used or your user incorrect token. Please connect with developer!!!");
                    return false;
                }
                $userId = User::checkUserDataByToken($email, $password, $token[4]);
                if (!$userId) {
                    echo json_encode("User not Find");
                    return false;
                } else {
                    User::auth($userId);
                    User::updateStatusUser($userId);
                    User::updateStatusToken($token[4]);
                    return true;
                }
            }
            $this->view->render('Camagru | Login');
        } else {
            $this->view->redirect('/');
        }
    }
}
